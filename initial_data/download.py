import requests

url = 'https://api-v2.soundcloud.com/users/170413650/tracks?client_id=L1Tsmo5VZ0rup3p9fjY67862DyPiWGaG&limit=1000'
finished = False

s = requests.session()
i = 1
while not finished:
    res = s.get(url)
    with open(f'tracks{i}.json', 'w') as f:
        f.write(res.text)
    content = res.json()
    url = content['next_href']
    print('Next URL:', url)
    i += 1

    if not url or len(content['collection']) < 1000:
        finished=True
