import requests
import hashlib
import sys
import json
import logging
from google.cloud import firestore

logging.getLogger().setLevel(logging.DEBUG)

db = firestore.Client()


artwork = requests.session()
def hashartwork(url):
    if url:
        res = artwork.get(url)
        return hashlib.sha256(res.content).hexdigest()
    else:
        return hashlib.sha256(b'').hexdigest()

def upload(track):
    db.collection('track').document(str(track['id'])).set(track)

for fn in sys.argv[1:]:
    with open(fn, 'r') as f:
        data = json.load(f)
    
    for i, track in enumerate(data['collection'],1):
        logging.info(f"Hashing {track['artwork_url']}")
        track['channel'] = 'sc-misykat'
        track['artwork_hash'] = hashartwork(track['artwork_url'])
        logging.info(f"Artwork hash: {track['artwork_hash']}")
        logging.info(f"Uploading track #{track['id']} ({i} of {len(data['collection'])}) {i*100/(len(data['collection'])):.2f}%")
        upload(track)
