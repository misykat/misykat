import requests
import hashlib
import sys
import json
import logging
from google.cloud import firestore

logging.getLogger().setLevel(logging.DEBUG)

db = firestore.Client()


for fn in sys.argv[1:]:
    with open(fn, 'r') as f:
        data = json.load(f)
    
    for i, track in enumerate(data['collection'],1):
        logging.info(f"Updating track #{track['id']} ({i} of {len(data['collection'])}) {i*100/(len(data['collection'])):.2f}%")
        db.collection('track').document(str(track['id'])).update({'channel': 'sc-misykat'})
    