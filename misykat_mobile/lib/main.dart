import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:open_appstore/open_appstore.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp() {
    () async {
      await initializeDateFormatting();
    }();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kajian Islam dan Doa',
      theme: ThemeData(primarySwatch: Colors.red),
      home: KajianPage(title: 'Kajian-Kajian'),
    );
  }
}

class KajianPage extends StatefulWidget {
  KajianPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _KajianPageState createState() => _KajianPageState();
}

class _KajianPageState extends State<KajianPage> {
  bool _searchShown = false;
  var _searchController =
      TextEditingController.fromValue(TextEditingValue(text: ''));
  bool _showLoading = false;
  Future<List<Track>> _future = Track.fetchTracks(http.Client());
  Timer _timer;
  Track currentTrack;

  @override
  void initState() {
    super.initState();

    _checkVersion();
  }

  void _checkVersion() async {
    var versionInfo = await VersionInfo.checkVersion(http.Client());

    if (versionInfo.installedVersionState == -1)
      return;
    else
      _showUpdateDialog(
          force: versionInfo.installedVersionState == 1,
          updateDescription: versionInfo.description);
  }

  void _showUpdateDialog({bool force, String updateDescription}) {
    var buttons = List<Widget>();
    if (!force) {
      buttons.add(FlatButton(
        child: Text("Nanti"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ));
    }
    buttons.add(FlatButton(
      child: Text("Ya"),
      onPressed: () {
        OpenAppstore.launch(androidAppId: "com.majulahijabi.misykat_mobile");
      },
    ));
    showDialog(
      context: context,
      barrierDismissible: !force,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text("Pembaruan/Update"),
            content: Text("Terdapat pembaruan/update untuk aplikasi ini\n" +
                "Apakah anda ingin membarukan sekarang?\n\n" +
                (updateDescription ?? "")),
            actions: buttons);
      },
    );
  }

  void _refreshTracks() async {
    setState(() => _showLoading = true);
    _future = Track.fetchTracks(http.Client());
    await _future;
    setState(() => _showLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
            appBar: AppBar(
              flexibleSpace: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: <Color>[Color(0x99f7ff00), Color(0xffdb36a4)])),
              ),
              title: !_searchShown
                  ? Text(widget.title)
                  : TextField(
                      decoration: InputDecoration(
                          hintText: 'Masukkan Kata Kunci Pencarian',
                          border: InputBorder.none),
                      autofocus: true,
                      controller: _searchController,
                      onChanged: (s) {
                        if (_timer != null) {
                          _timer.cancel();
                        }
                        _timer = Timer(Duration(milliseconds: 200),
                            () => setState(() => ''));
                      },
                    ),
              actions: !_searchShown
                  ? [
                      IconButton(
                        icon: const Icon(Icons.refresh),
                        onPressed: _refreshTracks,
                        splashColor: Colors.white,
                      ),
                      IconButton(
                        icon: const Icon(Icons.search),
                        onPressed: () {
                          setState(() {
                            _searchShown = true;
                          });
                        },
                      ),
                    ]
                  : [
                      IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () => setState(() {
                          _searchController.text = '';
                          _searchShown = false;
                        }),
                      )
                    ],
                    bottom: TabBar(
                      tabs: [
                        Tab(text: 'Suara',),
                        Tab(text: 'Video',),
                        Tab(text: 'Buku'),
                      ],
                    )
            ),
            body: buildBody()));
  }

  Widget buildBody() {
    var mainBody = TabBarView(
              children: [
                suaraTab(),
                videoTab(),
                bukuTab(),
              ]);
    if (currentTrack != null) {
      var viewer = buildViewer();
      return Stack(children: [
        Column(children: <Widget>[
          Expanded(child: mainBody),
          Container(height: viewer.getHeight(), child: viewer)
        ]),
        Padding(
          padding: EdgeInsets.only(bottom: viewer.getHeight()-15, right: 8),
          child: Align(
            alignment: Alignment.bottomRight,
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.black87,
              foregroundColor: Colors.white,
              child: IconButton(
                icon: Icon(Icons.close, size: 20),
                padding: EdgeInsets.all(0),
                onPressed: () {
                  setState(() {
                    currentTrack = null;
                  });
                },
            ))
        ))
      ]);
    } else {
      return mainBody;
    }
  }

  Viewer buildViewer() {
    switch (currentTrack.channel) {
      case "sc-misykat":
        return SoundCloudViewer(currentTrack);
      case "yt-misykat":
      case "yt-majulahijabi":
        return YouTubeViewer(currentTrack);
    }
    return null;
  }

  Widget suaraTab() {
    return FutureBuilder<List<Track>>(
        key: Key("suara_future"),
        future: _future,
        builder: (context, snapshot) {
            if (snapshot.hasError) {
              print(snapshot.error);
              return Center(
                  child: Text(
                      'Maaf, terjadi masalah dalam mengambil data. Silahkan periksa koneksi anda dan coba lagi dengan tombol refresh di atas'));
            } else if (!snapshot.hasData || _showLoading) {
              return Center(child: CircularProgressIndicator());
            }

            return Hero(
                tag: Key('suaratracklist'),
                child: TrackList(
                    tracks: snapshot.data,
                    searchFilter: _searchShown ? _searchController.text : "",
                    onChange: (track) {
                      setState(() => currentTrack = null);
                      setState(() => currentTrack = track);
                    }));
        });
  }

  Track videoSample = Track(
    5283617253,
    "yt-majulahijabi",
    DateTime.parse("2019-05-31T22:31:47.000Z"),
    "'UJUB MERUSAK IMAN Oleh KH DR Jalaluddin Rakhmat",
    ['test'],
    "https://i.ytimg.com/vi/V7zjTS3PwCU/default.jpg",
    "https://i.ytimg.com/vi/V7zjTS3PwCU/default.jpg",
    "V7zjTS3PwCU",
    560
  );
  Widget videoTab() {
    return TrackList(
      tracks: [videoSample],
      searchFilter: _searchShown ? _searchController.text : "",
      onChange: (track) {
        setState(() => currentTrack = null);
        setState(() => currentTrack = track);
      }
    );
  }

  Widget bukuTab() {
    return Center(child: Text('Sedang dalam pengembangan'));
  }
}

abstract class Viewer extends StatefulWidget {
  Viewer({Key key}): super(key: key);

  double getHeight();
}

class SoundCloudViewer extends Viewer {
  final Track track;

  SoundCloudViewer(this.track, {Key key}) : super(key: key ?? Key(track.trackUrl));

  @override
  State<SoundCloudViewer> createState() {
    return SoundCloudViewerState();
  }

  double getHeight() => 120;
}

class SoundCloudViewerState extends State<SoundCloudViewer> {
  bool loading = true;
  Completer t;

  @override
  Widget build(BuildContext context) {
    var embedUrl =
        "https://w.soundcloud.com/player/?url=${widget.track.trackUrl}&color=ff6600&auto_play=true&show_artwork=false&show_teaser=false";
    var wv = WebView(
        initialUrl: embedUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onPageFinished: (s) {
          print(s);
          setState(() => loading = false);
        });
    return IndexedStack(
      index: loading ? 1 : 0,
      children: [wv, Center(child: CircularProgressIndicator())],
    );
  }
}

class YouTubeViewer extends Viewer {
  final Track track;

  YouTubeViewer(this.track, {Key key}) : super(key: key ?? Key(track.trackUrl));
  @override
  State<StatefulWidget> createState() {
    return YouTubeViewerState();
  }

  double getHeight() => 240;
}

class YouTubeViewerState extends State<YouTubeViewer> {
  bool loading = true;
  Completer t;

  @override
  Widget build(BuildContext context) {
    var embedUrl =
        "https://www.youtube.com/embed/${widget.track.trackUrl}?autoplay=1";
    var wv = WebView(
        initialUrl: embedUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onPageFinished: (s) {
          print(s);
          setState(() => loading = false);
        });
    return IndexedStack(
      index: loading ? 1 : 0,
      children: [wv, Center(child: CircularProgressIndicator())],
    );
  }
}

class TrackList extends StatelessWidget {
  final List<Track> tracks;
  final String searchFilter;
  final ValueChanged<Track> onChange;

  const TrackList({Key key, this.tracks, this.searchFilter, this.onChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var s = searchFilter.toLowerCase();
    var c = (String a) => a.toLowerCase().contains(s);
    var filteredTracks =
        tracks.where((track) => c(track.title) || track.tags.any(c)).toList();
    filteredTracks.sort((a, b) => -a.date.compareTo(b.date));
    return ListView.builder(
        padding: EdgeInsets.all(5.0),
        itemCount: filteredTracks.length,
        itemBuilder: (context, index) {
          return TrackTile(
              track: filteredTracks[index],
              onSelect: () => onChange(filteredTracks[index]));
        });
  }
}

class TrackTile extends StatelessWidget {
  static Map<String, String> artworkUrls = Map();
  static var dateF = DateFormat.yMMMMEEEEd("id_ID");
  static var timeF = DateFormat.Hm();
  final Track track;
  final Function onSelect;

  const TrackTile({Key key, this.track, this.onSelect}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var artworkUrl =
        artworkUrls.putIfAbsent(track.artworkHash, () => track.artworkUrl);
    var channelLabel = 'SoundCloud Misykat'; // TODO: hardcoded sc-misykat
    var channelColor = Color(0x99ff4400); // TODO: hardcoded sc-misykat
    var itoa = (int i) => '$i'.padLeft(2, '0');
    return Card(
        key: Key('Track ${track.channel} #${track.id}'),
        color: Colors.white,
        child: InkWell(
            onTap: onSelect,
            child: Stack(children: [
              Padding(
                  padding: EdgeInsets.all(5),
                  child: Row(children: <Widget>[
                    Stack(children: [
                      (artworkUrl != null && artworkUrl != '')
                          ? CachedNetworkImage(
                              imageUrl: artworkUrl,
                              placeholder: (context, url) => Container(
                                  width: 84,
                                  height: 84,
                                  padding: EdgeInsets.all(24),
                                  child: CircularProgressIndicator()),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                              width: 84,
                              height: 84)
                          //Image.network(artworkUrl, width:84, height:84)
                          : SizedBox(
                              width: 84, height: 84, child: Text('No Image')),
                      Positioned(
                          bottom: 2,
                          right: 2,
                          child: Container(
                              color: Color.fromARGB(128, 0, 0, 0),
                              padding: EdgeInsets.all(2),
                              child: Text(
                                '${track.duration ~/ 1000 ~/ 60}:${itoa(track.duration ~/ 1000 % 60)}',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 255, 255, 255),
                                    fontSize: 12),
                              )))
                    ]),
                    Expanded(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(12, 0, 0, 14),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(track.title),
                                  Text(''),
                                  Text('${dateF.format(track.date)}'),
                                ])))
                  ])),
              Positioned(
                  right: 0,
                  bottom: 0,
                  child: ClipRRect(
                      borderRadius:
                          BorderRadius.only(bottomRight: Radius.circular(2)),
                      child: Container(
                          color: channelColor,
                          padding: EdgeInsets.all(2),
                          child: Text(channelLabel,
                              style: TextStyle(
                                  color: Color(0xffffffff), fontSize: 10)))))
            ])));
  }
}

/// data classes
class Track {
  final int id;
  final String channel;
  final DateTime date;
  final String title;
  final List<String> tags;
  final String artworkUrl;
  final String artworkHash;
  final String trackUrl;
  final int duration;

  Track(this.id, this.channel, this.date, this.title, this.tags,
      this.artworkUrl, this.artworkHash, this.trackUrl, this.duration);

  Track.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        channel = json['channel'],
        date = DateTime.parse(json['display_date']),
        title = json['title'],
        tags =
            json['tags'] == null ? <String>[] : List<String>.from(json['tags']),
        artworkUrl = json['artwork_url'],
        artworkHash = json['artwork_hash'],
        trackUrl = json['track_url'],
        duration = json['duration'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'channel': channel,
        'date': date.toIso8601String(),
        'title': title,
        'tags': tags,
        'artwork_url': artworkUrl,
        'artwork_hash': artworkHash,
        'track_url': trackUrl,
        'duration': duration
      };

  static List<Track> parseJsonTracks(String respBody) {
    final parsed = jsonDecode(respBody).cast<Map<String, dynamic>>();

    return parsed.map<Track>((json) {
      return Track.fromJson(json);
    }).toList();
  }

  static Future<List<Track>> fetchTracks(http.Client client) async {
    final res = await client
        .get('https://asia-east2-misykat.cloudfunctions.net/GetTracks');

    return await compute(parseJsonTracks, res.body);
  }

  static Future<List<Track>> fetchDummyTracks() async {
    await Future.delayed(Duration(seconds: 30));

    return <Track>[
      Track(
          1,
          "sc-misykat",
          DateTime.parse('2019-08-31T08:00:00'),
          "Mencari Jalan Nabi",
          ["test", "tost"],
          "",
          "",
          "http://www.google.com",
          12300)
    ];
  }
}

class VersionInfo {
  final String version;
  final DateTime mustUpdateDate;
  final String description;
  int installedVersionState;

  VersionInfo(this.version, this.mustUpdateDate, this.description);
  VersionInfo.fromJson(Map<String, dynamic> json)
      : version = json['version'],
        mustUpdateDate = DateTime.parse(json['must_update_date']),
        description = json['description'];
  Map<String, dynamic> toJson() => {
        'version': version,
        'must_update_date': mustUpdateDate.toIso8601String(),
        'description': description
      };

  static VersionInfo parseJson(String respBody) {
    final parsed = jsonDecode(respBody);

    return VersionInfo.fromJson(parsed);
  }

  ///return -1 kalau tidak perlu update, 0 kalau bisa dan 1 kalau harus update
  static Future<VersionInfo> checkVersion(http.Client client) async {
    PackageInfo packageInfo;
    http.Response res;

    var packageInfoF = () async {
      packageInfo = await PackageInfo.fromPlatform();
    };
    var resF = () async {
      res = await client
          .get('https://asia-east2-misykat.cloudfunctions.net/GetVersion');
    };
    // awaitnya bareng2
    await Future.wait([resF(), packageInfoF()]);

    var installedVersionStr = packageInfo.version;
    if (packageInfo.buildNumber != null && packageInfo.buildNumber != "")
      installedVersionStr += "+" + packageInfo.buildNumber;
    final latestInfo = VersionInfo.parseJson(res.body);
    final latest = Version.parse(latestInfo.version);
    final installed = Version.parse(installedVersionStr);

    var ret = (int state) {
      latestInfo.installedVersionState = state;
      return latestInfo;
    };
    if (installed >= latest) {
      return ret(-1);
    } else if (latest.major > installed.major ||
        latest.minor > installed.minor + 1 ||
        latest.patch > installed.minor + 3 ||
        latestInfo.mustUpdateDate.isBefore(DateTime.now())) {
      return ret(1);
    } else {
      return ret(0);
    }
  }
}
