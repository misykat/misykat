module gitlab.com/misykat/misykat/misykat_server

go 1.11

require (
	cloud.google.com/go/firestore v1.1.1
	google.golang.org/api v0.14.0
)
