package misykat

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"

	//"fmt"
	"context"
	"crypto/sha256"
	"log"
	"net/http"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
)

// ClientTrackInfo adalah informasi track info untuk dikembalikan ke client
type ClientTrackInfo struct {
	ID          int64     `json:"id"`
	Channel     string    `json:"channel"`
	DisplayDate time.Time `json:"display_date"`
	Title       string    `json:"title"`
	Tags        []string  `json:"tags"`
	ArtworkURL  string    `json:"artwork_url"`
	ArtworkHash string    `json:"artwork_hash"`
	TrackURL    string    `json:"track_url"`
}

func processTagList(tagList string) []string {
	var ret []string

	fields := strings.Fields(tagList)
	tmpstr := ""
	for i := 0; i < len(fields); i++ {
		if strings.HasPrefix(fields[i], "\"") {
			tmpstr = fields[i][1:]
		} else if tmpstr != "" {
			tmpstr += " " + fields[i]
		}
		if strings.HasSuffix(tmpstr, "\"") {
			ret = append(ret, tmpstr[:len(tmpstr)-1])
			tmpstr = ""
		}
	}
	return ret
}

func refreshTracksIfNeeded() {
	projectID := "misykat"

	// Get a Firestore client.
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
		panic(err)
	}

	// Close client when done.
	defer client.Close()
	// TODO: mengeceknya jangan di setiap request, ditambahin random saja
	//       supaya hit ke DB lebih sedikit (bayaar euy)
	dsnap, err := client.Collection("channel").Doc("sc-misykat").Get(ctx)
	if err != nil {
		panic(err)
	}
	var lastChecked string
	mustCheck := false
	now := time.Now()
	if dsnap.Exists() {
		lc, err := dsnap.DataAt("last_checked")
		if err != nil {
			panic(err)
		}
		lastChecked = lc.(string)
		lastCheckedT, err := time.Parse(time.RFC3339, lastChecked)
		if err != nil {
			panic(err)
		}
		if now.Sub(lastCheckedT) > 60*60*1000000 {
			mustCheck = true
		}
	} else {
		mustCheck = true
	}
	if mustCheck {
		log.Println("Checking misykat soundcloud:", lastChecked)
		client.Collection("channel").Doc("sc-misykat").Set(ctx, map[string]interface{}{"last_checked": now.Format(time.RFC3339)})
		err := refreshSoundCloud(lastChecked)
		if err != nil {
			// kalau error, kembalikan lastChecked
			client.Collection("channel").Doc("sc-misykat").Set(ctx, map[string]interface{}{"last_checked": lastChecked})
		}
	}
}

// GetTracks adalah fungsi handler utama untuk mengembalikan trackss
func GetTracks(w http.ResponseWriter, r *http.Request) {
	refreshTracksIfNeeded()

	projectID := "misykat"

	// Get a Firestore client.
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
		panic(err)
	}

	// Close client when done.
	defer client.Close()

	var tracks []map[string]interface{}

	iter := client.Collection("track").Documents(ctx)
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			panic(err)
		}

		trackmap := doc.Data()
		track := map[string]interface{}{
			"id":           trackmap["id"],
			"channel":      trackmap["channel"],
			"display_date": trackmap["display_date"],
			"title":        trackmap["title"],
			"tags":         processTagList(trackmap["tag_list"].(string)),
			"artwork_url":  trackmap["artwork_url"],
			"artwork_hash": trackmap["artwork_hash"],
			"track_url":    trackmap["permalink_url"],
			"duration":     trackmap["duration"],
			// track url: embed
			//https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/101276036&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=true"
		}

		tracks = append(tracks, track)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(tracks); err != nil {
		panic(err)
	}
}

func hashArtwork(url string) (string, error) {
	var hash string
	h := sha256.Sum256([]byte(""))
	hash = hex.EncodeToString(h[0:32])

	if url != "" {
		res, err := http.Get(url)
		if err != nil {
			return hash, err
		}
		defer res.Body.Close()
		h := sha256.New()

		if _, err := io.Copy(h, res.Body); err != nil {
			return hash, err
		}
		hash = hex.EncodeToString(h.Sum(nil))
	}
	return hash, nil
}

// RefreshSoundCloud mengambil data dari soundcloud dan mengupdate ke collection track
func refreshSoundCloud(lastChecked string) error {
	// TODO
	var content map[string]interface{}

	url := "https://api-v2.soundcloud.com/users/170413650/tracks?client_id=qeWb21nmKO1VUDsY88W1341i7kO1JXeK&limit=10000"
	res, err := http.Get(url)
	if err != nil {
		print("Error refresh soundcloud: error connecting")
		return err
	}
	defer res.Body.Close()

	//json.Unmarshal
	err = json.NewDecoder(res.Body).Decode(&content)
	if err != nil {
		print("Error refresh soundcloud: error decoding body")
		return err
	}

	contentCollection := content["collection"].([]interface{})
	tracks := make(map[string]map[string]interface{})
	for i := range contentCollection {
		track := contentCollection[i].(map[string]interface{})
		if track["last_modified"].(string) > lastChecked {
			tracks[fmt.Sprintf("%.0f", track["id"].(float64))] = track
		}
	}
	if len(tracks) == 0 {
		return nil
	}

	projectID := "misykat"
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		print("Error connecting to firestore")
		return err
	}

	for trackID, track := range tracks {
		track["channel"] = "sc-misykat"
		if track["artwork_url"] == nil {
			track["artwork_hash"], err = hashArtwork("")
		} else {
			track["artwork_hash"], err = hashArtwork(track["artwork_url"].(string))
		}
		client.Collection("track").Doc(trackID).Set(ctx, track)
	}
	return nil
}

// GetVersion akan mengembalikan json versi terbaru dan harus diupdate kapan
func GetVersion(w http.ResponseWriter, r *http.Request) {
	version := struct {
		Version     string `json:"version"`
		MustUpdate  string `json:"must_update_date"`
		Description string `json:"description"`
	}{"1.0.1+5", "2020-02-03T23:24:15Z",
		"Update 1.1.0:\n" +
			"- Penambahan bagian Video\n" +
			"- Channel baru: YouTube Majulah Ijabi",
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(version); err != nil {
		panic(err)
	}
}
