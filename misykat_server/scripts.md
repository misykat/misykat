Deploy Script
-------------

```
gcloud functions deploy GetTracks --runtime go111 --trigger-http --verbosity=debug --region=asia-east2
```